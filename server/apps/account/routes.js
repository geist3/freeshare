var auth = require("./auth");
var users = require("./controllers/users");

module.exports = function(config){
    
    config.app.get('/api/users', auth.requiresRole('admin'), users.getUsers);
    config.app.post('/api/users', users.createUser);
    config.app.put('/api/users', users.updateUser);
    
    config.app.all('/logout', function(req, res){
       req.logout();
       res.end();
    });
    
    config.app.post('/login', auth.authenticate);
};