var mongoose = require("mongoose");
var Item = mongoose.model("Item");

exports.getItems = function(req, res){
    return Item.find({}).populate('owner').exec(function(err, items){
        res.send(items);
    });
};

exports.addItem = function(req, res){
  var itemData = req.body;
  Item.create(itemData, function(err, item){
     if(err){
         res.status = 400;
         res.send({success:false, reason:err.toString()});
     }else{
         res.send(item);
     }
  });
};