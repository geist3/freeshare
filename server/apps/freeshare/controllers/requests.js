var mongoose = require("mongoose");
var Request = mongoose.model("Request");
var Item = mongoose.model("Item");

exports.getRequests = function(req, res){
    return Request.find({}).populate('borrower item').exec(function(err, requests){
        res.send(requests);
    });
};

exports.addRequest = function(req, res){
  var itemId = req.body.itemId;
  var requestData = {};
  Item.findOne({"_id": itemId}, function(err, item){
    if(err){
      res.send({status: 400, reason: 'Couldnt find item'});
    }else{
      requestData.item = item._id;
      requestData.borrower = req.user._id;
      Request.create(requestData, function(err, request){
         if(err){
             res.status = 400;
             res.send({success:false, reason:err.toString()});
         }else{
             res.send(request);
         }
      });
    }
  });
};

exports.deleteRequest = function(req, res){
  var itemId = req.query.id;
  console.log('delete', itemId, req.user._id);
  Request.remove({item: itemId, borrower:req.user._id}, function(err){
    if(err){
      res.send({status: 400, reason: 'err'});
    }else{
        res.send(true);
    }
  });
};