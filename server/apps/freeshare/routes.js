var items = require("./controllers/items");
var requests = require("./controllers/requests");
var auth = require("../account/auth");
var routes = function(config){
    config.app.get('/partials/item/:partialsPath', function(req, res){
      res.render(__dirname + '/views/partials/item/' + req.params.partialsPath);  
    });

    config.app.get('/partials/request/:partialsPath', function(req, res){
      res.render(__dirname + '/views/partials/request/' + req.params.partialsPath);  
    });

    config.app.get('/partials/:partialsPath', function(req, res){
      res.render(__dirname + '/views/partials/' + req.params.partialsPath);  
    });
    
    config.app.get('/', function(req, res) {
        res.render(__dirname + '/views/freeshare/index', {
            bootstrappedUser:  req.user
        });
    });
    
    config.app.get('/api/item', items.getItems);
    config.app.post('/api/item', auth.requiresApiLogin, items.addItem);

    config.app.get('/api/request', requests.getRequests);
    config.app.post('/api/request', auth.requiresApiLogin, requests.addRequest);
    config.app.delete('/api/request', auth.requiresApiLogin, requests.deleteRequest);
};

module.exports = routes;
