var path = require('path');
var express = require('express');
var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');
var stylus = require('stylus');

module.exports = function(config){
    config.express = express;
    config.app = new express();
    config.app.set('view engine', 'jade');
    config.app.set('views', path.join(config.rootDir, 'server', 'views'));
    
    config.app.use(config.express.static(path.join(config.rootDir, 'public')));
    config.app.use(cookieParser());
    config.app.use(bodyParser.json());
    config.app.use(bodyParser.urlencoded({
      extended: true
    }));
    config.app.use(session({secret: 'sdffeffrrtrthjikmnbcxs445tg5ff', resave:true, saveUninitialized: true}));
    
    function compileCss(str, path){
        return stylus(str).set('filename', path);
    }

    config.app.use(stylus.middleware({
        src: config.rootDir + '/public/css',
        compile:compileCss
    }));
};
