var env = process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var config = {
    ip: process.env.IP || '127.0.0.1',
    env: env
};

if(config.env == 'development'){
    config.port = process.env.PORT || 3000;
} else {
    config.port = process.env.PORT || 80;
}

module.exports = config;