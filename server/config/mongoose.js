var mongoose = require('mongoose');
var userModel = require('../models/user');
var itemModel = require('../models/item');
var requestModel = require('../models/request');
var User = mongoose.model('User');
var Item = mongoose.model('Item');
var Request = mongoose.model('Request');
var encrypt = require('../utilities/encryption');

module.exports = function(config){
    console.log('connecting to db');

    mongoose.connect('mongodb://localhost/freeshare');
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error...'));
    db.once('open', function callback(){
        console.log('db open');
    });
    
    User.find({}).exec(function(err, collection){
        if(collection.length === 0){
            var salt = encrypt.createSalt();
            var hash = encrypt.hashPwd(salt, 'john');
            User.create({firstName:'John', lastName:'Cockrell', username:'j@j.com', salt:salt, hashed_pwd: hash, roles: ['admin']}, function(err, john){
                if(err){
                    console.log(err);
                }else{
                    salt = encrypt.createSalt();
                    hash = encrypt.hashPwd(salt, 'user');
                    User.create({firstName:'user', lastName:'user', username:'user@user.com', salt:salt, hashed_pwd: hash},function(err, user){
                        if(err){
                            console.log(err);
                        }else{
                            Item.create({
                                name:'BladeRunner', 
                                description:'Best movie ever. Blue ray', 
                                status: 'available', 
                                owner:john._id,
                                borrower: undefined,
                                tags: ['blue-ray', 'film', 'sci-fi']
                            }, function(err, item){
                                if(err){
                                    console.log(err);
                                }else{
                                    Request.create({
                                       item:item._id,
                                       borrower:user._id
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};