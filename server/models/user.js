var mongoose = require('mongoose');
var encrypt = require('../utilities/encryption');

var userSchema = mongoose.Schema({
    firstName: {type:String, required:"{PATH} is required"},
    lastName: {type:String, required:"{PATH} is required"},
    username: {
        type:String, 
        required:"{PATH} is required",
        unique:true
    },
    salt: String,
    hashed_pwd: String,
    roles: [String]
});

userSchema.methods = {
    authenticate: function(passwordToMatch){
        return encrypt.hashPwd(this.salt, passwordToMatch) == this.hashed_pwd;
    },
    hasRole: function(role){
      return this.roles.indexOf(role) > -1;  
    },
    isAuthenticated: function(){
        return true;
    }
};
    
var User = mongoose.model('User', userSchema);
