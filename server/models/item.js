var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var itemSchema = mongoose.Schema({
    name: {type:String, required:"{PATH} is required"},
    description:String,
    status:String,
    owner: {type: ObjectId, ref: 'User'},
    borrower: {type: ObjectId, ref: 'User'},
    tags: [String]
});

var Item = mongoose.model('Item', itemSchema);