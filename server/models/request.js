var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

var requestSchema = mongoose.Schema({
    item: {type: ObjectId, ref: 'Item'},
    borrower: {type: ObjectId, ref: 'User'},
    status: String
});

var Request = mongoose.model('Request', requestSchema);