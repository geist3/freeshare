angular.module('app').controller('mvItemAddCtrl', function($scope, mvItem, $location, mvNotifier, mvItemAPI){
    $scope.addItem = function(){
        var newItemData = {
            name: $scope.name,
            description: $scope.description,
            tags: $scope.tags
        };
        
        mvItemAPI.createItem(newItemData).then(function(){
            mvNotifier.notify("success");
            $location.path("/items");
        }, function(reason){
            mvNotifier.error(reason);
        });
    };
});