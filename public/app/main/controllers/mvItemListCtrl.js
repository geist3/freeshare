angular.module('app').controller('mvItemListCtrl', function($scope, mvItem, mvIdentity, mvRequestAPI){
   $scope.identity = mvIdentity; 
   $scope.requests = mvRequestAPI.query();
   $scope.items = mvItem.query();
});