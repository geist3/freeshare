angular.module('app').factory('mvItemAPI', function($resource, $q, mvItem){
    return {
        createItem : function(newItemData){
            var newItem = new mvItem(newItemData);
            var dfd = $q.defer();
            
            newItem.$save().then(function()
                {
                    dfd.resolve();
                }, function(response){
                    dfd.reject(response.data.reason);
                }
            );
            
            return dfd.promise;
        }
    }
})