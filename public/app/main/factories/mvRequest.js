angular.module('app').factory('mvRequest', function($resource){
    var ItemResource = $resource('/api/request', {_id: '@id'});
    return ItemResource;
})