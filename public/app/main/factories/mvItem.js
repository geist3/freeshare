angular.module('app').factory('mvItem', function($resource){
    var ItemResource = $resource('/api/item', {_id: '@id'});
    return ItemResource;
})