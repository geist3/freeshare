angular.module('app').factory('mvRequestAPI', function($resource, $q, mvRequest, $http){
    var requests;
    
    return {
        createRequest : function(newRequestData){
            var newRequest = new mvRequest(newRequestData);
            var dfd = $q.defer();
            
            newRequest.$save().then(function(data)
                {
                    console.log(data);
                    dfd.resolve(data);
                }, function(response){
                    dfd.reject(response.data.reason);
                }
            );
            
            return dfd.promise;
        },
        deleteRequest : function(request){
            var dfd = $q.defer();
            request.$delete().then(function()
                {
                    dfd.resolve();
                }, function(response){
                    dfd.reject(response.data.reason);
                }
            );
            
            return dfd.promise;
        },
        query : function(){
            if(!requests){
                requests = mvRequest.query();
            }
            
            return requests;
        },
        queryOne : function(item){
            var request;
            if (requests && requests.$resolved) {
                console.log('b1', requests);
                $.each(requests, function(i, v){
                   if(v.item._id == item._id){
                       request = v;
                       return false;
                   } 
                });
                console.log('b2', request);
            } else {
                requests = mvRequest.query(function(requests){
                    $.each(requests, function(i, v){
                       if(v.item._id == item._id){
                           request = v;
                           return false;
                       } 
                    });
                    console.log('a', request);
                });
            }
            
            return request;
        }
    }
})