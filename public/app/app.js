angular.module('app', ['ngResource','ngRoute']);

angular.module('app').config(function($routeProvider, $locationProvider){
    var routeRoleChecks = {
        admin : {auth: function(mvAuth) {
            return mvAuth.authorizeCurrentUserForRoute('admin');
        }},
        user: {auth: function(mvAuth){
            return mvAuth.authorizeAuthenticatedUserForRoute()
        }}
    }

    $locationProvider.html5Mode(false);
    $routeProvider
        .when('/', {
            templateUrl: 'partials/main',
            controller:'mvMainCtrl'
            
        })
        .when('/admin/users', {
            templateUrl: 'partials/user-list', 
            controller:'mvUserListCtrl', 
            resolve:routeRoleChecks.admin
        })
        .when('/items', {
            templateUrl: 'partials/item/list',
            controller: 'mvItemListCtrl'
        })
        .when('/requests', {
            templateUrl: 'partials/request/list',
            controller: 'mvRequestListCtrl'
        })
        .when('/addItem', {
            templateUrl: 'partials/item/add',
            resolve: routeRoleChecks.user,
            controller: 'mvItemAddCtrl'
        })
        .when('/signup', {
            templateUrl: 'partials/signup', 
            controller:'mvSignupCtrl'
        })
        .when('/profile', {
            templateUrl: 'partials/profile',
            resolve: routeRoleChecks.user,
            controller:'mvProfileCtrl'
        });
});

angular.module('app').run(function($rootScope, $location){
    $rootScope.$on('$routeChangeError', function(evt, current, previous, rejection){
        if(rejection === 'not authorized'){
            $location.path("/");
        }
    })
})
