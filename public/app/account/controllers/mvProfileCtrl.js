angular.module('app').controller('mvProfileCtrl', function($scope, mvAuth, mvNotifier, mvIdentity){
    console.log('b', mvIdentity.currentUser.username, mvIdentity.currentUser);
    $scope.email = mvIdentity.currentUser.username;
    $scope.fname = mvIdentity.currentUser.firstName;
    $scope.lname = mvIdentity.currentUser.lastName;
    
    $scope.update = function(){
        var newUserData = {
            username: $scope.email,
            firstname: $scope.fname,
            lastname: $scope.lname
        }
        
        if($scope.password && $scope.password.length > 0){
            newUserData.password = $scope.password;
        }
        
        mvAuth.updateCurrentUser(newUserData).then(function(){
            mvNotifier.notify('Updated');
        }, function(reason) {
            mvNotifier.error(reason);
        })
    }
})