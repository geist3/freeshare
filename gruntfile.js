module.exports = function(grunt){
    grunt.initConfig({
        jshint: {
            jsFiles:['server/**/*.js']
        },
        stylus: {
            compile: {
                files: {
                    'public/css/main.css': ['public/css/main.styl']
                }
            }
        },
        
        uglify: {
            options: {
              mangle: false,
              sourceMap:true
            },
            my_target: {
              files: {
                'public/js/ng.js': [
                    'public/app/app.js',
                    'public/app/common/mvNotifier.js',
                    'public/app/main/controllers/mvMainCtrl.js', 
                    'public/app/account/controllers/mvNavLoginCtrl.js',
                    'public/app/account/factories/mvIdentity.js',
                    'public/app/account/factories/mvAuth.js',
                    'public/app/account/factories/mvUser.js',
                    'public/app/account/controllers/mvSignupCtrl.js',
                    'public/app/account/controllers/mvProfileCtrl.js',
                    'public/app/admin/controllers/mvUserListCtrl.js',
                    'public/app/main/factories/mvItem.js',
                    'public/app/main/factories/mvRequest.js',
                    'public/app/main/factories/mvItemAPI.js',
                    'public/app/main/factories/mvRequestAPI.js',
                    'public/app/main/controllers/mvItemListCtrl.js',
                    'public/app/main/controllers/mvRequestListCtrl.js',
                    'public/app/main/controllers/mvItemAddCtrl.js',
                    'public/app/main/controllers/mvTopNavLinksCtrl.js'
                    ]
              }
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-contrib-stylus');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    
    grunt.registerTask('default', ['jshint','stylus', 'uglify']);
};