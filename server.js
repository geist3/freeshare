var config = require('./server/config/config');

config.rootDir = __dirname;

require('./server/config/express')(config);
require('./server/config/mongoose')(config);
require('./server/config/passport')(config);

require('./server/apps/freeshare/routes')(config);
require('./server/apps/account/routes')(config);

config.app.listen(config.port, config.ip);
console.log('Listening on port ' + config.port + ' and ip ' + config.ip)
